#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <gsl/gsl_rng.h>
#include <sys/time.h>

#ifdef PARALLEL
#include <omp.h>
#else

int omp_get_thread_num(void)
{
	return 0;
}

#endif

#include "config.h"
#include "mc.h"
#include "auxx.h"

#define LOCAL_PATH	"."
#define CLUSTER_PATH	"/localhome/gbighin/"

bool running_via_slurm(void)
{
	if(getenv("SLURM_JOBID")!=NULL)
		return true;

	return false;
}

int do_phase_diagram_bilayer(void)
{
	struct configuration_t config;
	char fname[1024];
	FILE *outfile;
	int count,total;

	config.x=config.y=32;
	config.nrlayers=2;

	config.nrJs=2;
	config.nrJperps=1;
	config.beta=1.0f;

	config.runs=1;
	config.thermalization=250;
	config.sampling=3000;
	config.decorrelation=5;

	config.inipath=NULL;
	config.verbose=false;
	config.savesnapshots=true;

	config.model=MODEL_ISING_NLAYER;
	config.updates=UPDATES_SW;

	if(running_via_slurm()==true)
		snprintf(fname,1024,"%s/nl.dat",CLUSTER_PATH);
	else
		snprintf(fname,1024,"%s/nl.dat",LOCAL_PATH);

	fname[1023]='\0';

	outfile=fopen(fname,"w+");
	count=0;
	total=(1+50)*(1+140);

	fprintf(stderr,"Phase diagram: Ising bilayer\n");

#pragma omp parallel for collapse(2) schedule(dynamic)

	for(int centiJ=0;centiJ<=50;centiJ++)
	{
		for(int centiK=0;centiK<=140;centiK++)
		{
			gsl_rng *rng_ctx;
			struct configuration_t localconfig;
			char prefix[1024];
			struct timeval starttime,endtime;
			double elapsed_time;

			memcpy(&localconfig,&config,sizeof(struct configuration_t));

			localconfig.Js[0]=localconfig.Js[1]=0.01*centiJ;
			localconfig.Jperps[0]=0.01*centiK;

			if(running_via_slurm()==true)
				snprintf(prefix,1024,"%s/nl.J%d.K%d",CLUSTER_PATH,centiJ,centiK);
			else
				snprintf(prefix,1024,"%s/nl.J%d.K%d",LOCAL_PATH,centiJ,centiK);

			prefix[1023]='\0';
			localconfig.prefix=prefix;

			rng_ctx=gsl_rng_alloc(gsl_rng_mt19937);
			assert(rng_ctx!=NULL);
			seed_rng(rng_ctx);

			gettimeofday(&starttime,NULL);
			do_mc(&localconfig,rng_ctx,outfile,((centiJ==2)&&(centiK==2))?(true):(false));
			gettimeofday(&endtime,NULL);

			elapsed_time=(endtime.tv_sec-starttime.tv_sec)*1000.0;
			elapsed_time+=(endtime.tv_usec-starttime.tv_usec)/1000.0;
			elapsed_time/=1000;

#pragma omp critical
			{
				fprintf(stderr,"Ising bilayer: betaJ=%f, betaK=%f (%d/%d)",0.01*centiJ,0.01*centiK,++count,total);
				fprintf(stderr," (%f seconds, thread #%d)\n",elapsed_time,omp_get_thread_num());
				fflush(stderr);
			}

			gsl_rng_free(rng_ctx);
		}
	}

	if(outfile)
		fclose(outfile);

	return 0;
}

int do_phase_diagram_ashkin_teller(void)
{
	struct configuration_t config;
	char fname[1024];
	FILE *outfile;
	int count,total;

	config.x=config.y=32;
	config.nrlayers=2;

	config.nrJs=2;
	config.nrJperps=1;
	config.beta=1.0f;

	config.runs=1;
	config.thermalization=25000;
	config.sampling=6000;
	config.decorrelation=10;

	config.inipath=NULL;
	config.verbose=false;
	config.savesnapshots=true;

	config.model=MODEL_ASHKIN_TELLER;
	config.updates=UPDATES_SW;

	if(running_via_slurm()==true)
		snprintf(fname,1024,"%s/at.dat",CLUSTER_PATH);
	else
		snprintf(fname,1024,"%s/at.dat",LOCAL_PATH);

	fname[1023]='\0';

	outfile=fopen(fname,"w+");
	count=0;
	total=(1+80)*(1+80);

	fprintf(stderr,"Phase diagram: Ashkin-Teller\n");

#pragma omp parallel for collapse(2) schedule(dynamic)

	for(int centiJ=0;centiJ<=80;centiJ++)
	{
		for(int centiK=0;centiK<=80;centiK++)
		{
			gsl_rng *rng_ctx;
			struct configuration_t localconfig;
			char prefix[1024];
			struct timeval starttime,endtime;
			double elapsed_time;

			memcpy(&localconfig,&config,sizeof(struct configuration_t));

			localconfig.Js[0]=localconfig.Js[1]=0.01*centiJ;
			localconfig.Jperps[0]=0.01*centiK;

			if(running_via_slurm()==true)
				snprintf(prefix,1024,"%s/at.J%d.K%d",CLUSTER_PATH,centiJ,centiK);
			else
				snprintf(prefix,1024,"%s/at.J%d.K%d",LOCAL_PATH,centiJ,centiK);

			prefix[1023]='\0';
			localconfig.prefix=prefix;

			rng_ctx=gsl_rng_alloc(gsl_rng_mt19937);
			assert(rng_ctx!=NULL);
			seed_rng(rng_ctx);

			gettimeofday(&starttime,NULL);
			do_mc(&localconfig,rng_ctx,outfile,((centiJ==2)&&(centiK==2))?(true):(false));
			gettimeofday(&endtime,NULL);

			elapsed_time=(endtime.tv_sec-starttime.tv_sec)*1000.0;
			elapsed_time+=(endtime.tv_usec-starttime.tv_usec)/1000.0;
			elapsed_time/=1000;

#pragma omp critical
			{
				fprintf(stderr,"Ashkin-Teller: betaJ=%f, betaK=%f (%d/%d)",0.01*centiJ,0.01*centiK,++count,total);
				fprintf(stderr," (%f seconds, thread #%d)\n",elapsed_time,omp_get_thread_num());
				fflush(stderr);
			}

			gsl_rng_free(rng_ctx);
		}
	}

	return 0;
}

int do_phase_diagram_trilayer(void)
{
	struct configuration_t config;
	char fname[1024];
	FILE *outfile;
	int count,total;

	config.x=config.y=32;
	config.nrlayers=3;

	config.nrJs=3;
	config.nrJperps=2;
	config.beta=1.0f;

	config.runs=1;
	config.thermalization=250;
	config.sampling=3000;
	config.decorrelation=5;

	config.inipath=NULL;
	config.verbose=false;
	config.savesnapshots=true;

	config.model=MODEL_ISING_NLAYER;
	config.updates=UPDATES_SW;

	if(running_via_slurm()==true)
		snprintf(fname,1024,"%s/nl3.dat",CLUSTER_PATH);
	else
		snprintf(fname,1024,"%s/nl3.dat",LOCAL_PATH);

	fname[1023]='\0';

	outfile=fopen(fname,"w+");
	count=0;
	total=(1+50)*(1+140);

	fprintf(stderr,"Phase diagram: Ising trilayer\n");

#pragma omp parallel for collapse(2) schedule(dynamic)

	for(int centiJ=0;centiJ<=50;centiJ++)
	{
		for(int centiK=0;centiK<=140;centiK++)
		{
			gsl_rng *rng_ctx;
			struct configuration_t localconfig;
			char prefix[1024];
			struct timeval starttime,endtime;
			double elapsed_time;

			memcpy(&localconfig,&config,sizeof(struct configuration_t));

			localconfig.Js[0]=localconfig.Js[1]=localconfig.Js[2]=0.01*centiJ;
			localconfig.Jperps[0]=localconfig.Jperps[1]=0.01*centiK;

			if(running_via_slurm()==true)
				snprintf(prefix,1024,"%s/nl3.J%d.K%d",CLUSTER_PATH,centiJ,centiK);
			else
				snprintf(prefix,1024,"%s/nl3.J%d.K%d",LOCAL_PATH,centiJ,centiK);

			prefix[1023]='\0';
			localconfig.prefix=prefix;

			rng_ctx=gsl_rng_alloc(gsl_rng_mt19937);
			assert(rng_ctx!=NULL);
			seed_rng(rng_ctx);

			gettimeofday(&starttime,NULL);
			do_mc(&localconfig,rng_ctx,outfile,((centiJ==2)&&(centiK==2))?(true):(false));
			gettimeofday(&endtime,NULL);

			elapsed_time=(endtime.tv_sec-starttime.tv_sec)*1000.0;
			elapsed_time+=(endtime.tv_usec-starttime.tv_usec)/1000.0;
			elapsed_time/=1000;

#pragma omp critical
			{
				fprintf(stderr,"Ising trilayer: betaJ=%f, betaK=%f (%d/%d)",0.01*centiJ,0.01*centiK,++count,total);
				fprintf(stderr," (%f seconds, thread #%d)\n",elapsed_time,omp_get_thread_num());
				fflush(stderr);
			}

			gsl_rng_free(rng_ctx);
		}
	}

	if(outfile)
		fclose(outfile);

	return 0;
}

int do_phase_diagram(void)
{
	do_phase_diagram_bilayer();
	do_phase_diagram_ashkin_teller();
	do_phase_diagram_trilayer();

	return 0;
}

int do_phase_diagram_ashkin_teller_short(int dimensions)
{
	struct configuration_t config;
	char fname[1024];
	FILE *outfile;
	int count,total;

	config.x=config.y=dimensions;
	config.nrlayers=2;

	config.nrJs=2;
	config.nrJperps=1;
	config.beta=1.0f;

	config.runs=1;
	config.thermalization=25000;
	config.sampling=6000;
	config.decorrelation=10;

	config.inipath=NULL;
	config.verbose=false;
	config.savesnapshots=true;

	config.model=MODEL_ASHKIN_TELLER;
	config.updates=UPDATES_SW;

	if(running_via_slurm()==true)
		snprintf(fname,1024,"%s/atshort%d.dat",CLUSTER_PATH,dimensions);
	else
		snprintf(fname,1024,"%s/atshort%d.dat",LOCAL_PATH,dimensions);

	fname[1023]='\0';

	outfile=fopen(fname,"w+");
	count=0;
	total=(1)*(1+80);

	fprintf(stderr,"Phase diagram: Ashkin-Teller (short, dimensions=%d)\n",dimensions);

#pragma omp parallel for collapse(2) schedule(dynamic)

	for(int centiJ=2;centiJ<=80;centiJ++)
	{
		for(int centiK=70;centiK<=70;centiK++)
		{
			gsl_rng *rng_ctx;
			struct configuration_t localconfig;
			char prefix[1024];
			struct timeval starttime,endtime;
			double elapsed_time;

			memcpy(&localconfig,&config,sizeof(struct configuration_t));

			localconfig.Js[0]=localconfig.Js[1]=0.01*centiJ;
			localconfig.Jperps[0]=0.01*centiK;

			if(running_via_slurm()==true)
				snprintf(prefix,1024,"%s/at%d.J%d.K%d",CLUSTER_PATH,dimensions,centiJ,centiK);
			else
				snprintf(prefix,1024,"%s/at%d.J%d.K%d",LOCAL_PATH,dimensions,centiJ,centiK);

			prefix[1023]='\0';
			localconfig.prefix=prefix;

			rng_ctx=gsl_rng_alloc(gsl_rng_mt19937);
			assert(rng_ctx!=NULL);
			seed_rng(rng_ctx);

			gettimeofday(&starttime,NULL);
			do_mc(&localconfig,rng_ctx,outfile,((centiJ==2)&&(centiK==70))?(true):(false));
			gettimeofday(&endtime,NULL);

			elapsed_time=(endtime.tv_sec-starttime.tv_sec)*1000.0;
			elapsed_time+=(endtime.tv_usec-starttime.tv_usec)/1000.0;
			elapsed_time/=1000;

#pragma omp critical
			{
				fprintf(stderr,"Ashkin-Teller (short): betaJ=%f, betaK=%f (%d/%d)",0.01*centiJ,0.01*centiK,++count,total);
				fprintf(stderr," (%f seconds, thread #%d)\n",elapsed_time,omp_get_thread_num());
				fflush(stderr);
			}

			gsl_rng_free(rng_ctx);
		}
	}

	return 0;
}

int main(int argc,char **argv)
{
	assert(validate_jpermutations()==true);

	if((argc==2)&&(strcmp(argv[1],"--phasediagram1")==0))
		return do_phase_diagram_bilayer();

	if((argc==2)&&(strcmp(argv[1],"--phasediagram2")==0))
		return do_phase_diagram_ashkin_teller();

	if((argc==2)&&(strcmp(argv[1],"--phasediagram3")==0))
		return do_phase_diagram_trilayer();

	if((argc==2)&&(strcmp(argv[1],"--phasediagram4")==0))
	{
		do_phase_diagram_ashkin_teller_short(16);
		do_phase_diagram_ashkin_teller_short(24);
		do_phase_diagram_ashkin_teller_short(32);
		do_phase_diagram_ashkin_teller_short(48);

		return 0;
	}

	if(argc<2)
	{
		printf("Usage: %s <inifile> [<otherinifiles> ...]\n",argv[0]);
		return 0;
	}

	for(int c=1;c<argc;c++)
		do_ini_file(argv[c]);

	return 0;
}
