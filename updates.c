#include <assert.h>
#include <math.h>

#include "nlayer.h"
#include "clusters.h"
#include "bonds.h"
#include "auxx.h"
#include "updates.h"

/*
	A MC step, following the Swendsen-Wang algorithm, for an Ising n-layer system.
*/

int sw_nlayer_step(struct nlayer_t *nl, gsl_rng *rng_ctx)
{
	struct nclusters_t *nclusters;
	int nr_clusters;

	nclusters=nclusters_init(nl->lx,nl->ly,nl->nrlayers);
	assert(nclusters);

	/*
		We stochastically build the bonds between different neighbouring
		sites, sitting on the same layer...
	*/

	for(int c=0;c<nl->nrlayers;c++)
	{
		nclusters->bonds[c]=ibond2d_init(nl->lx,nl->ly);

		for(int x=0;x<nl->lx;x++)
		{
			for(int y=0;y<nl->ly;y++)
			{
				ibond2d_set_value(nclusters->bonds[c],x,y,DIR_X,0);

				/*
					If two nearest neighbours spins have the same sign,
					then a bond is activated with probability p.
				*/

				if(nlayer_get_spin(nl,x,y,c)==nlayer_get_spin(nl,(x+1)%nl->lx,y,c))
				{
					double p=1.0f-exp(-2.0f*nl->beta*nl->Js[c]);

					if(gsl_rng_uniform(rng_ctx)<p)
						ibond2d_set_value(nclusters->bonds[c],x,y,DIR_X,1);
				}

				ibond2d_set_value(nclusters->bonds[c],x,y,DIR_Y,0);

				/*
					Same, along the y axis.
				*/

				if(nlayer_get_spin(nl,x,y,c)==nlayer_get_spin(nl,x,(y+1)%nl->lx,c))
				{
					double p=1.0f-exp(-2.0f*nl->beta*nl->Js[c]);

					if(gsl_rng_uniform(rng_ctx)<p)
						ibond2d_set_value(nclusters->bonds[c],x,y,DIR_Y,1);
				}
			}
		}
	}

	/*
		...and there are also 'vertical' bonds, between different layers.
	*/

	for(int l=0;l<(nl->nrlayers-1);l++)
	{
		nclusters->ivbonds[l]=ivbond2d_init(nl->lx,nl->ly);

		for(int x=0;x<nl->lx;x++)
		{
			for(int y=0;y<nl->ly;y++)
			{
				ivbond2d_set_value(nclusters->ivbonds[l],x,y,0);

				if(nlayer_get_spin(nl,x,y,l)==nlayer_get_spin(nl,x,y,l+1))
				{
					double p=1.0f-exp(-2.0f*nl->beta*nl->Jperps[l]);

					if(gsl_rng_uniform(rng_ctx)<p)
						ivbond2d_set_value(nclusters->ivbonds[l],x,y,1);
				}
			}
		}
	}

	/*
		We now identify the clusters...
	*/

	nr_clusters=nclusters_identify(nclusters);

	/*
		...and then flip them with random probabilities!
	*/

	for(int c=1;c<nr_clusters;c++)
	{
		if(gsl_rng_uniform(rng_ctx)<0.5f)
			nclusters_reset(nl,nclusters,c,1);
		else
			nclusters_reset(nl,nclusters,c,-1);
	}

	/*
		Final cleanups
	*/

	for(int c=0;c<nl->nrlayers;c++)
		ibond2d_fini(nclusters->bonds[c]);

	for(int l=0;l<(nl->nrlayers-1);l++)
		ivbond2d_fini(nclusters->ivbonds[l]);

	nclusters_fini(nclusters);

	/*
		The number of flipped spins, i.e. the total number of spins
		in the lattice, is returned.
	*/

	return nl->lx*nl->ly*nl->nrlayers;
}

/*
	A MC step, following the Wolff algorithm, for an Ising n-layer system.
*/

int wolff_nlayer_step(struct nlayer_t *nl, gsl_rng *rng_ctx)
{
	struct nclusters_t *nclusters;
	int spins_in_cluster;

	nclusters=nclusters_init(nl->lx,nl->ly,nl->nrlayers);
	assert(nclusters);

	/*
		TODO: for the Wolff algorithm we don't have to construct all the bonds.
		Therefore, this code could be optimized by calculating and checking the bonds
		as we expand the Wolff cluster...
	*/

	/*
		We stochastically build the bonds between different neighbouring
		sites, sitting on the same layer...
	*/

	for(int c=0;c<nl->nrlayers;c++)
	{
		nclusters->bonds[c]=ibond2d_init(nl->lx,nl->ly);

		for(int x=0;x<nl->lx;x++)
		{
			for(int y=0;y<nl->ly;y++)
			{
				ibond2d_set_value(nclusters->bonds[c],x,y,DIR_X,0);

				/*
					If two nearest neighbours spins have the same sign,
					then a bond is activated with probability p.
				*/

				if(nlayer_get_spin(nl,x,y,c)==nlayer_get_spin(nl,(x+1)%nl->lx,y,c))
				{
					double p=1.0f-exp(-2.0f*nl->beta*nl->Js[c]);

					if(gsl_rng_uniform(rng_ctx)<p)
						ibond2d_set_value(nclusters->bonds[c],x,y,DIR_X,1);
				}

				ibond2d_set_value(nclusters->bonds[c],x,y,DIR_Y,0);

				/*
					Same, along the y axis.
				*/

				if(nlayer_get_spin(nl,x,y,c)==nlayer_get_spin(nl,x,(y+1)%nl->lx,c))
				{
					double p=1.0f-exp(-2.0f*nl->beta*nl->Js[c]);

					if(gsl_rng_uniform(rng_ctx)<p)
						ibond2d_set_value(nclusters->bonds[c],x,y,DIR_Y,1);
				}
			}
		}
	}

	/*
		...and there are also 'vertical' bonds, between different layers.
	*/

	for(int l=0;l<(nl->nrlayers-1);l++)
	{
		nclusters->ivbonds[l]=ivbond2d_init(nl->lx,nl->ly);

		for(int x=0;x<nl->lx;x++)
		{
			for(int y=0;y<nl->ly;y++)
			{
				ivbond2d_set_value(nclusters->ivbonds[l],x,y,0);

				if(nlayer_get_spin(nl,x,y,l)==nlayer_get_spin(nl,x,y,l+1))
				{
					double p=1.0f-exp(-2.0f*nl->beta*nl->Jperps[l]);

					if(gsl_rng_uniform(rng_ctx)<p)
						ivbond2d_set_value(nclusters->ivbonds[l],x,y,1);
				}
			}
		}
	}

	/*
		We now select a random spin and grow a cluster starting from there.
	*/

	for(int x=0;x<nclusters->lx;x++)
		for(int y=0;y<nclusters->ly;y++)
			for(int l=0;l<nclusters->nrlayers;l++)
				nclusters_set_value(nclusters,x,y,l,0);

	spins_in_cluster=nclusters_grow(nclusters,gsl_rng_uniform_int(rng_ctx,nl->lx),
		                                  gsl_rng_uniform_int(rng_ctx,nl->ly),
		                                  gsl_rng_uniform_int(rng_ctx,nl->nrlayers),19);

	/*
		At last the cluster is flipped with random probability!
	*/

	if(gsl_rng_uniform(rng_ctx)<0.5f)
		nclusters_reset(nl,nclusters,19,1);
	else
		nclusters_reset(nl,nclusters,19,-1);

	/*
		Final cleanups
	*/

	for(int c=0;c<nl->nrlayers;c++)
		ibond2d_fini(nclusters->bonds[c]);

	for(int l=0;l<(nl->nrlayers-1);l++)
		ivbond2d_fini(nclusters->ivbonds[l]);

	nclusters_fini(nclusters);

	return spins_in_cluster;
}

int actual_wolff_nlayer_step(struct nlayer_t *nl, gsl_rng *rng_ctx)
{
	struct stack_t *stack;
	struct nlayer_t *visited;
	int spins_in_cluster,cluster_spin,lx,ly;

	stack=stack_init();

	stack_push(stack,gsl_rng_uniform_int(rng_ctx,nl->lx),
		         gsl_rng_uniform_int(rng_ctx,nl->ly),
		         gsl_rng_uniform_int(rng_ctx,nl->nrlayers));

	spins_in_cluster=0;
	cluster_spin=nlayer_get_spin(nl,stack->data[0],stack->data[1],stack->data[2]);

	lx=nl->lx;
	ly=nl->ly;

	visited=nlayer_init(nl->lx,nl->ly,nl->nrlayers,nl->Js,nl->Jperps,nl->beta);

	for(int x=0;x<visited->lx;x++)
		for(int y=0;y<visited->ly;y++)
			for(int l=0;l<visited->nrlayers;l++)
				nlayer_set_spin(visited,x,y,l,0);

	/*
		We go on until the stack is not empty.
	*/

	while(stack->size>0)
	{
		int x,y,l;

		stack_pop(stack,&x,&y,&l);

		assert(x>=0);
		assert(y>=0);
		assert(l>=0);
		assert(x<lx);
		assert(y<ly);
		assert(l<nl->nrlayers);

		if(nlayer_get_spin(nl,x,y,l)!=cluster_spin)
			continue;

		if(nlayer_get_spin(visited,x,y,l)==1)
			continue;

		nlayer_set_spin(visited,x,y,l,1);

		nlayer_flip_spin(nl,x,y,l);
		spins_in_cluster++;

		double p=1.0f-exp(-2.0f*nl->beta*nl->Js[l]);

		if(gsl_rng_uniform(rng_ctx)<p)
			stack_push(stack,(x+1)%lx,y,l);

		if(gsl_rng_uniform(rng_ctx)<p)
			stack_push(stack,x,(y+1)%ly,l);

		if(gsl_rng_uniform(rng_ctx)<p)
			stack_push(stack,(x+lx-1)%lx,y,l);

		if(gsl_rng_uniform(rng_ctx)<p)
			stack_push(stack,x,(y+ly-1)%ly,l);

		if((l+1)<nl->nrlayers)
		{
			p=1.0f-exp(-2.0f*nl->beta*nl->Jperps[l]);

			if(gsl_rng_uniform(rng_ctx)<p)
				stack_push(stack,x,y,l+1);
		}

		if(l>0)
		{
			p=1.0f-exp(-2.0f*nl->beta*nl->Jperps[l-1]);

			if(gsl_rng_uniform(rng_ctx)<p)
				stack_push(stack,x,y,l-1);
		}
	}

	nlayer_fini(visited);

	stack_fini(stack);

	return spins_in_cluster;
}

/*
	A MC step, following the Swendsen-Wang algorithm, for an Ashkin-Teller system,
	and auxiliary functions.

	See for instance: https://arxiv.org/pdf/hep-lat/9511022.pdf
*/

void get_mij_nij(int sigmai,int sigmaj,int taui,int tauj,double z,int *mij,int *nij,
		 double p1,double p2,double p3,double q1,double r1)
{
	if((sigmai==sigmaj)&&(taui==tauj))
	{
		if(z<p1)
		{
			*mij=1;
			*nij=1;
		}
		else if(z<(p1+p2))
		{
			*mij=1;
			*nij=0;
		}
		else if(z<(p1+p2+p3))
		{
			*mij=0;
			*nij=1;
		} else
		{
			*mij=0;
			*nij=0;
		}
	}
	else if((sigmai==sigmaj)&&(taui==-tauj))
	{
		if(z<q1)
		{
			*mij=1;
			*nij=0;
		}
		else
		{
			*mij=0;
			*nij=0;
		}
	}
	else if((sigmai==-sigmaj)&&(taui==tauj))
	{
		if(z<r1)
		{
			*mij=0;
			*nij=1;
		}
		else
		{
			*mij=0;
			*nij=0;
		}
	}
	else if((sigmai==-sigmaj)&&(taui==-tauj))
	{
		*mij=0;
		*nij=0;
	}
	else
	{
		assert(false);
	}
}

int sw_ashkin_teller_step(struct nlayer_t *nl,gsl_rng *rng_ctx)
{
	struct nclusters_t *m,*n;
	int nr_clusters;

	double p1,p2,p3,p4,q1,q2,r1,r2;
	double J,Jprime,K;

	assert(nl->nrlayers==2);

	J=nl->beta*nl->Js[0];
	Jprime=nl->beta*nl->Js[1];
	K=nl->beta*nl->Jperps[0];

	m=nclusters_init(nl->lx,nl->ly,1);
	n=nclusters_init(nl->lx,nl->ly,1);
	assert((n!=NULL)&&(m!=NULL));

	m->bonds[0]=ibond2d_init(nl->lx,nl->ly);
	n->bonds[0]=ibond2d_init(nl->lx,nl->ly);

	p1=1.0-exp(-2.0f*(Jprime+K))-exp(-2.0f*(J+K))+exp(-2.0f*(J+Jprime));
	p2=exp(-2.0f*Jprime)*(exp(-2.0f*K)-exp(-2.0f*J));
	p3=exp(-2.0f*J)*(exp(-2.0f*K)-exp(-2.0f*Jprime));
	p4=exp(-2.0f*(J+Jprime));

	q1=1.0f-exp(-2.0f*(J+K));
	q2=exp(-2.0f*(J+K));

	r1=1.0f-exp(-2.0f*(Jprime+K));
	r2=exp(-2.0f*(Jprime+K));

	assert((p1>=0)&&(p2>=0)&&(p3>=0)&&(p4>=0));
	assert((q1>=0)&&(q2>=0));
	assert((r1>=0)&&(r2>=0));

	assert(almost_same_double(p1+p2+p3+p4,1.0f));
	assert(almost_same_double(q1+q2,1.0f));
	assert(almost_same_double(r1+r2,1.0f));

	/*
		At first we create the bond variables defined on the dual lattice...
	*/

	for(int x=0;x<nl->lx;x++)
	{
		for(int y=0;y<nl->ly;y++)
		{
			int sigmai,sigmaj,taui,tauj;
			int mij,nij;
			double z;

			/* Let's silence gcc warnings */

			mij=nij=0;

			/* Bonds across the x direction */

			sigmai=nlayer_get_spin(nl,x,y,0);
			taui=nlayer_get_spin(nl,x,y,1);
			sigmaj=nlayer_get_spin(nl,(x+1)%nl->lx,y,0);
			tauj=nlayer_get_spin(nl,(x+1)%nl->lx,y,1);

			z=gsl_rng_uniform(rng_ctx);
			get_mij_nij(sigmai,sigmaj,taui,tauj,z,&mij,&nij,p1,p2,p3,q1,r1);

			ibond2d_set_value(m->bonds[0],x,y,DIR_X,mij);
			ibond2d_set_value(n->bonds[0],x,y,DIR_X,nij);

			/* Bonds across the y direction */

			sigmai=nlayer_get_spin(nl,x,y,0);
			taui=nlayer_get_spin(nl,x,y,1);
			sigmaj=nlayer_get_spin(nl,x,(y+1)%nl->ly,0);
			tauj=nlayer_get_spin(nl,x,(y+1)%nl->ly,1);

			z=gsl_rng_uniform(rng_ctx);
			get_mij_nij(sigmai,sigmaj,taui,tauj,z,&mij,&nij,p1,p2,p3,q1,r1);

			ibond2d_set_value(m->bonds[0],x,y,DIR_Y,mij);
			ibond2d_set_value(n->bonds[0],x,y,DIR_Y,nij);
		}
	}

	/*
		...and then we flip the spins according to the clusters just created.
	*/

	nr_clusters=nclusters_identify(m);
	for(int c=1;c<nr_clusters;c++)
	{
		int value=(gsl_rng_uniform(rng_ctx)<0.5f)?(1):(-1);

		for(int x=0;x<m->lx;x++)
			for(int y=0;y<m->ly;y++)
				if(nclusters_get_value(m,x,y,0)==c)
					ising2d_set_spin(nl->layers[0],x,y,value);
	}

	nr_clusters=nclusters_identify(n);
	for(int c=1;c<nr_clusters;c++)
	{
		int value=(gsl_rng_uniform(rng_ctx)<0.5f)?(1):(-1);

		for(int x=0;x<n->lx;x++)
			for(int y=0;y<n->ly;y++)
				if(nclusters_get_value(n,x,y,0)==c)
					ising2d_set_spin(nl->layers[1],x,y,value);
	}

	ibond2d_fini(n->bonds[0]);
	ibond2d_fini(m->bonds[0]);

	nclusters_fini(n);
	nclusters_fini(m);

	/*
		The number of flipped spins, i.e. the total number of spins
		in the lattice, is returned.
	*/

	return nl->lx*nl->ly*nl->nrlayers;
}

/*
	A MC step, following the Wolff algorithm, for the Ashkin-Teller model.

	As opposed as in the previous function, here we use the embedding idea presented
	in Section 3.2 of hep-lat/9511022, see also Phys. Rev. E 51, 3074 (1994).
*/

int wolff_ashkin_teller_step(struct nlayer_t *nl,gsl_rng *rng_ctx)
{
	struct nclusters_t *nclusters;
	int thislayer,otherlayer,spins_in_cluster;

	/*
		Symmetries must be used to ensure that these conditions are always met,
		see hep-lat/9511022, Section 2 and 3 and Baxter's book.
	*/

	assert(nl->nrlayers==2);
	assert(nl->Js[0]>=fabs(nl->Jperps[0]));
	assert(nl->Js[1]>=fabs(nl->Jperps[0]));

	/*
		TODO: for the Wolff algorithm we don't have to construct all the bonds.
		Therefore, this code could be optimized by calculated checking the bonds as we expand
		the Wolff cluster...
	*/

	nclusters=nclusters_init(nl->lx,nl->ly,1);
	assert(nclusters);

	thislayer=gsl_rng_uniform_int(rng_ctx,2);
	otherlayer=(thislayer==0)?(1):(0);

	nclusters->bonds[0]=ibond2d_init(nl->lx,nl->ly);

	for(int x=0;x<nl->lx;x++)
	{
		for(int y=0;y<nl->ly;y++)
		{
			ibond2d_set_value(nclusters->bonds[0],x,y,DIR_X,0);

			/*
				If two nearest neighbours spins have the same sign,
				then a bond is activated with probability p.
			*/

			if(nlayer_get_spin(nl,x,y,thislayer)==nlayer_get_spin(nl,(x+1)%nl->lx,y,thislayer))
			{
				double Jeff=nl->Js[thislayer]+nl->Jperps[0]*nlayer_get_spin(nl,x,y,otherlayer)*nlayer_get_spin(nl,(x+1)%nl->lx,y,otherlayer);
				double p=1.0f-exp(-2.0f*nl->beta*Jeff);

				assert(Jeff>=0.0f);

				if(gsl_rng_uniform(rng_ctx)<p)
					ibond2d_set_value(nclusters->bonds[0],x,y,DIR_X,1);
			}

			ibond2d_set_value(nclusters->bonds[0],x,y,DIR_Y,0);

			/*
				Same, along the y axis.
			*/

			if(nlayer_get_spin(nl,x,y,thislayer)==nlayer_get_spin(nl,x,(y+1)%nl->ly,thislayer))
			{
				double Jeff=nl->Js[thislayer]+nl->Jperps[0]*nlayer_get_spin(nl,x,y,otherlayer)*nlayer_get_spin(nl,x,(y+1)%nl->ly,otherlayer);
				double p=1.0f-exp(-2.0f*nl->beta*Jeff);

				assert(Jeff>=0.0f);

				if(gsl_rng_uniform(rng_ctx)<p)
					ibond2d_set_value(nclusters->bonds[0],x,y,DIR_Y,1);
			}
		}
	}

	/*
		We now select a random spin and grow a cluster starting from there.
	*/

	for(int x=0;x<nclusters->lx;x++)
		for(int y=0;y<nclusters->ly;y++)
			nclusters_set_value(nclusters,x,y,0,0);

	spins_in_cluster=nclusters_grow(nclusters,
					gsl_rng_uniform_int(rng_ctx,nl->lx),
					gsl_rng_uniform_int(rng_ctx,nl->ly),
					0,
					19);

	/*
		At last the cluster is flipped with random probability!
	*/

	int value=(gsl_rng_uniform(rng_ctx)<0.5f)?(1):(-1);

	for(int x=0;x<nl->lx;x++)
		for(int y=0;y<nl->ly;y++)
			if(nclusters_get_value(nclusters,x,y,0)==19)
				ising2d_set_spin(nl->layers[thislayer],x,y,value);

	/*
		Final cleanups
	*/

	ibond2d_fini(nclusters->bonds[0]);

	nclusters_fini(nclusters);

	return spins_in_cluster;
}

int actual_wolff_ashkin_teller_step(struct nlayer_t *nl,gsl_rng *rng_ctx)
{
	/*
		Symmetries must be used to ensure that these conditions are always met,
		see hep-lat/9511022, Section 2 and 3 and Baxter's book.
	*/

	assert(nl->nrlayers==2);
	assert(nl->Js[0]>=fabs(nl->Jperps[0]));
	assert(nl->Js[1]>=fabs(nl->Jperps[0]));

	int thislayer,otherlayer;

	thislayer=gsl_rng_uniform_int(rng_ctx,2);
	otherlayer=(thislayer==0)?(1):(0);

	struct stack_t *stack;
	struct ising2d_t *visited;
	int spins_in_cluster,cluster_spin,lx,ly;

	stack=stack_init();

	stack_push(stack,gsl_rng_uniform_int(rng_ctx,nl->lx),
		         gsl_rng_uniform_int(rng_ctx,nl->ly),
		         thislayer);

	spins_in_cluster=0;
	cluster_spin=nlayer_get_spin(nl,stack->data[0],stack->data[1],stack->data[2]);

	lx=nl->lx;
	ly=nl->ly;

	visited=ising2d_init(nl->lx,nl->ly);

	for(int x=0;x<visited->lx;x++)
		for(int y=0;y<visited->ly;y++)
				ising2d_set_spin(visited,x,y,0);

	/*
		We go on until the stack is not empty.
	*/

	while(stack->size>0)
	{
		int x,y,l;

		stack_pop(stack,&x,&y,&l);

		assert(x>=0);
		assert(y>=0);
		assert(l>=0);
		assert(x<lx);
		assert(y<ly);
		assert(l<nl->nrlayers);
		assert(l==thislayer);

		if(nlayer_get_spin(nl,x,y,l)!=cluster_spin)
			continue;

		if(ising2d_get_spin(visited,x,y)==1)
			continue;

		ising2d_set_spin(visited,x,y,1);

		nlayer_flip_spin(nl,x,y,l);
		spins_in_cluster++;

		double Jeff,p;

		Jeff=nl->Js[thislayer]+nl->Jperps[0]*nlayer_get_spin(nl,x,y,otherlayer)*nlayer_get_spin(nl,(x+1)%nl->lx,y,otherlayer);
		p=1.0f-exp(-2.0f*nl->beta*Jeff);

		if(gsl_rng_uniform(rng_ctx)<p)
			stack_push(stack,(x+1)%lx,y,l);

		Jeff=nl->Js[thislayer]+nl->Jperps[0]*nlayer_get_spin(nl,x,y,otherlayer)*nlayer_get_spin(nl,x,(y+1)%ly,otherlayer);
		p=1.0f-exp(-2.0f*nl->beta*Jeff);

		if(gsl_rng_uniform(rng_ctx)<p)
			stack_push(stack,x,(y+1)%ly,l);

		Jeff=nl->Js[thislayer]+nl->Jperps[0]*nlayer_get_spin(nl,x,y,otherlayer)*nlayer_get_spin(nl,(x+lx-1)%nl->lx,y,otherlayer);
		p=1.0f-exp(-2.0f*nl->beta*Jeff);

		if(gsl_rng_uniform(rng_ctx)<p)
			stack_push(stack,(x+lx-1)%lx,y,l);

		Jeff=nl->Js[thislayer]+nl->Jperps[0]*nlayer_get_spin(nl,x,y,otherlayer)*nlayer_get_spin(nl,x,(y+ly-1)%ly,otherlayer);
		p=1.0f-exp(-2.0f*nl->beta*Jeff);

		if(gsl_rng_uniform(rng_ctx)<p)
			stack_push(stack,x,(y+ly-1)%ly,l);
	}

	ising2d_fini(visited);

	stack_fini(stack);

	return spins_in_cluster;
}

double interaction_energy_xy(struct nlayer_t *nl,int x1,int y1,int z1,int x2,int y2,int z2)
{
	int s1,s2;

	assert(z1==z2);

	s1=nlayer_get_spin(nl,x1,y1,z1);
	s2=nlayer_get_spin(nl,x2,y2,z2);

	return -nl->Js[z1]*s1*s2;
}

#define MIN(x,y)	(((x)<(y))?(x):(y))

double interaction_energy_z(struct nlayer_t *nl,int x1,int y1,int z1,int x2,int y2,int z2)
{
	int s1,s2;

	assert(abs(z2-z1)==1);

	s1=nlayer_get_spin(nl,x1,y1,z1);
	s2=nlayer_get_spin(nl,x2,y2,z2);

	return -nl->Jperps[MIN(z1,z2)]*s1*s2;
}

int generic_metropolis_step(struct nlayer_t *nl,gsl_rng *rng_ctx)
{
	int x,y,z;

	x=gsl_rng_uniform_int(rng_ctx,nl->lx);
	y=gsl_rng_uniform_int(rng_ctx,nl->ly);
	z=gsl_rng_uniform_int(rng_ctx,nl->nrlayers);

	double deltae=0.0f;

	deltae-=interaction_energy_xy(nl,x,y,z,(x+1)%nl->lx,y,z);
	deltae-=interaction_energy_xy(nl,x,y,z,(x+nl->lx-1)%nl->lx,y,z);

	deltae-=interaction_energy_xy(nl,x,y,z,x,(y+1)%nl->ly,z);
	deltae-=interaction_energy_xy(nl,x,y,z,x,(y+nl->ly-1)%nl->ly,z);

	if(z>0) deltae-=interaction_energy_z(nl,x,y,z,x,y,z-1);
	if((z+1)<nl->nrlayers) deltae-=interaction_energy_z(nl,x,y,z,x,y,z+1);

	nlayer_flip_spin(nl,x,y,z);

	deltae+=interaction_energy_xy(nl,x,y,z,(x+1)%nl->lx,y,z);
	deltae+=interaction_energy_xy(nl,x,y,z,(x+nl->lx-1)%nl->lx,y,z);

	deltae+=interaction_energy_xy(nl,x,y,z,x,(y+1)%nl->ly,z);
	deltae+=interaction_energy_xy(nl,x,y,z,x,(y+nl->ly-1)%nl->ly,z);

	if(z>0) deltae+=interaction_energy_z(nl,x,y,z,x,y,z-1);
	if((z+1)<nl->nrlayers) deltae+=interaction_energy_z(nl,x,y,z,x,y,z+1);

	double boltzmann_factor=exp(-nl->beta*deltae);
	double p=MIN(1.0,boltzmann_factor);

	if(p<gsl_rng_uniform(rng_ctx))
	{
		nlayer_flip_spin(nl,x,y,z);
		return 0;
	}

	return 1;
}
