#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <stdbool.h>
#include <gsl/gsl_rng.h>

struct configuration_t
{
	int x,y,nrlayers;

#define MODEL_UNDEFINED		(-1)
#define MODEL_ISING_NLAYER	(15)
#define MODEL_ASHKIN_TELLER	(16)

	int model;

#define MAX_LATTICE_SIZE	(65536)
#define MAX_NR_OF_LAYERS 	(128)

	double Js[MAX_NR_OF_LAYERS],Jperps[MAX_NR_OF_LAYERS],beta;
	int nrJs,nrJperps;
	
	int runs,thermalization,sampling,decorrelation;
	char *inipath,*prefix;

#define UPDATES_SW	(101)
#define UPDATES_WOLFF	(102)

	int updates;
	bool savesnapshots;

	bool verbose;
};

void load_configuration_defaults(struct configuration_t *config);
bool check_configuration(struct configuration_t *config);
bool do_ini_file(char *inifile);

#endif //__CONFIG_H__
