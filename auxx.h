#ifndef __AUX_H__
#define __AUX_H__

#include <stdio.h>
#include <stdbool.h>
#include <gsl/gsl_rng.h>

char *find_and_replace(const char *src,const char *from,const char *to);
void fcopy(FILE *f1,FILE *f2);
void seed_rng(gsl_rng *rng);
bool almost_same_double(double a,double b);

#endif //__AUX_H__
