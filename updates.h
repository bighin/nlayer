#ifndef __UPDATES_H__
#define __UPDATES_H__

#include <gsl/gsl_rng.h>

#include "nlayer.h"

int sw_nlayer_step(struct nlayer_t *nl, gsl_rng *rng_ctx);
int wolff_nlayer_step(struct nlayer_t *nl, gsl_rng *rng_ctx);

int sw_ashkin_teller_step(struct nlayer_t *nl,gsl_rng *rng_ctx);
int wolff_ashkin_teller_step(struct nlayer_t *nl,gsl_rng *rng_ctx);

#endif //__UPDATES_H__
