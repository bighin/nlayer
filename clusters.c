#include <stdlib.h>
#include <assert.h>

#include "config.h"
#include "ising.h"
#include "bonds.h"
#include "clusters.h"
#include "nlayer.h"

/*
	Clusters on a nlayer!
*/

struct nclusters_t *nclusters_init(int x,int y,int nrlayers)
{
	struct nclusters_t *ret;
	
	assert(x>0);
	assert(y>0);
	assert(nrlayers>0);
	assert(nrlayers<MAX_NR_OF_LAYERS);

	if(!(ret=malloc(sizeof(struct nclusters_t))))
		return NULL;

	for(int c=0;c<nrlayers;c++)
	{
		if(!(ret->vals[c]=malloc(sizeof(int)*x*y)))
		{
			/*
				If one allocation fails, we free all the
				previous one and return.
			*/

			for(c--;c>=0;c--)
				if(ret->vals[c])
					free(ret->vals[c]);
			
			return NULL;
		}
	}

	ret->lx=x;
	ret->ly=y;
	ret->nrlayers=nrlayers;

	return ret;
}

void nclusters_fini(struct nclusters_t *nc)
{
	if(nc)
	{
		for(int c=0;c<nc->nrlayers;c++)
			if(nc->vals[c])
				free(nc->vals[c]);

		free(nc);
	}
}

int nclusters_get_value(struct nclusters_t *nclusters,int x,int y,int layer)
{
	assert(nclusters!=NULL);
	assert(x>=0);
	assert(y>=0);
	assert(layer>=0);
	assert(x<nclusters->lx);
	assert(y<nclusters->ly);
	assert(layer<nclusters->nrlayers);

	return nclusters->vals[layer][MAKE_INDEX(nclusters,x,y)];
}

void nclusters_set_value(struct nclusters_t *nclusters,int x,int y,int layer,int value)
{
	assert(nclusters!=NULL);
	assert(x>=0);
	assert(y>=0);
	assert(layer>=0);
	assert(x<nclusters->lx);
	assert(y<nclusters->ly);
	assert(layer<nclusters->nrlayers);

	nclusters->vals[layer][MAKE_INDEX(nclusters,x,y)]=value;
}

/*
	A simple stack object, used by the flood fill algorithm
*/

struct stack_t *stack_init(void)
{
	struct stack_t *stack;

	if(!(stack=malloc(sizeof(struct stack_t))))
		return NULL;

	stack->size=0;
	stack->alloced=1024;
	stack->data=malloc(stack->alloced*3*sizeof(int));

	if(!stack->data)
	{
		if(stack)
			free(stack);
		
		return NULL;
	}

	return stack;
}

void stack_push(struct stack_t *stack,int x,int y,int l)
{
	assert(stack);
	assert(stack->size<=stack->alloced);

	if(stack->size==stack->alloced)
	{
		stack->alloced+=1024;
		stack->data=realloc(stack->data,stack->alloced*3*sizeof(int));

		assert(stack->data);
	}

	stack->data[stack->size*3]=x;
	stack->data[stack->size*3+1]=y;
	stack->data[stack->size*3+2]=l;
	stack->size++;
}

void stack_pop(struct stack_t *stack,int *x,int *y,int *l)
{
	assert(stack);
	assert(stack->size<=stack->alloced);
	assert(stack->size>0);

	stack->size--;
	*x=stack->data[stack->size*3];
	*y=stack->data[stack->size*3+1];
	*l=stack->data[stack->size*3+2];
}

void stack_fini(struct stack_t *stack)
{
	if(stack)
	{
		if(stack->data)
			free(stack->data);
	
		free(stack);
	}
}

int nclusters_grow(struct nclusters_t *nclusters,int startx,int starty,int startl,int id)
{
	struct stack_t *stack;
	int count=0;

	assert(id!=0);

	/*
		Number 0 identifies a 'free' site where we can grow a cluster,
		whereas any other number means that that site is occupied.
	*/

	if(nclusters_get_value(nclusters,startx,starty,startl)!=0)
		return 0;

	/*
		If the first site is free, the we set up a stack, we push the
		first site on the stack and we are ready to go!
	*/

	stack=stack_init();
	stack_push(stack,startx,starty,startl);

	/*
		We go on until the stack is not empty.
	*/

	while(stack->size>0)
	{
		int x,y,l,lx,ly;

		/*
			We take a site from the stack, we check if we can keep on
			growing the cluster here, and if that's the case we push the
			neighbours on the stack, of course only if the bond between
			this site and the neighbour is active.
		*/

		stack_pop(stack,&x,&y,&l);
		lx=nclusters->lx;
		ly=nclusters->ly;

		assert(x>=0);
		assert(y>=0);
		assert(l>=0);
		assert(x<nclusters->lx);
		assert(y<nclusters->ly);
		assert(l<nclusters->nrlayers);
		assert(nclusters->lx==nclusters->bonds[l]->lx);
		assert(nclusters->ly==nclusters->bonds[l]->ly);

		if(nclusters_get_value(nclusters,x,y,l)!=0)
			continue;

		nclusters_set_value(nclusters,x,y,l,id);
		count++;

		if(ibond2d_get_value(nclusters->bonds[l],x,y,DIR_X)==1)
				stack_push(stack,(x+1)%lx,y,l);

		if(ibond2d_get_value(nclusters->bonds[l],x,y,DIR_Y)==1)
			stack_push(stack,x,(y+1)%ly,l);

		if(ibond2d_get_value(nclusters->bonds[l],(x+lx-1)%lx,y,DIR_X)==1)
			stack_push(stack,(x+lx-1)%lx,y,l);

		if(ibond2d_get_value(nclusters->bonds[l],x,(y+ly-1)%ly,DIR_Y)==1)
			stack_push(stack,x,(y+ly-1)%ly,l);

		if((l+1)<nclusters->nrlayers)
			if(ivbond2d_get_value(nclusters->ivbonds[l],x,y)==1)
				stack_push(stack,x,y,l+1);

		if(l>0)
			if(ivbond2d_get_value(nclusters->ivbonds[l-1],x,y)==1)
				stack_push(stack,x,y,l-1);
	}

	stack_fini(stack);

	return count;
}

#warning Here I should use the Hoshen-Kopelman algorithm, it is probably faster.

#define MAX_NR_OF_CLUSTERS	(4096)

int hk_find(const int labels[MAX_NR_OF_CLUSTERS],int x)
{
	while(labels[x]!=x)
		x=labels[x];

	return x;
}

int hk_union(int labels[MAX_NR_OF_CLUSTERS],int x,int y)
{
	return labels[hk_find(labels,x)]=hk_find(labels,y);
}

int max2(int x, int y)
{
	return x>y ? x : y;
}

int max3(int x, int y, int z)
{
	return max2(x, max2(y, z));
}

int nclusters_identify_hk(struct nclusters_t *nclusters)
{
	int labels[MAX_NR_OF_CLUSTERS];
	int id;

	for(int x=0;x<nclusters->lx;x++)
		for(int y=0;y<nclusters->ly;y++)
			for(int l=0;l<nclusters->nrlayers;l++)
				nclusters_set_value(nclusters,x,y,l,0);

	for(int c=0;c<MAX_NR_OF_CLUSTERS;c++)
		labels[c]=0;

	id=1;
	for(int x=0;x<nclusters->lx;x++)
	{
		for(int y=0;y<nclusters->ly;y++)
		{
			for(int l=0;l<nclusters->nrlayers;l++)
			{
				int neighbours[3];

				neighbours[0]=neighbours[1]=neighbours[2]=0;

				/*
					We determine if we have neighbours on each directions,
					only on sites we have already visited.
				*/

				if(x>0)
					if(ibond2d_get_value(nclusters->bonds[l],x-1,y,DIR_X)==1)
						neighbours[0]=nclusters_get_value(nclusters,x-1,y,l);

				if(y>0)
					if(ibond2d_get_value(nclusters->bonds[l],x,y-1,DIR_Y)==1)
						neighbours[1]=nclusters_get_value(nclusters,x,y-1,l);

				if(l>0)
					if(ivbond2d_get_value(nclusters->ivbonds[l-1],x,y)==1)
						neighbours[2]=nclusters_get_value(nclusters,x,y,l-1);

				/*
					Depending on how many neighbours we have,
					the current site is updated.
				*/

				switch((!!neighbours[0])+(!!neighbours[1])+(!!neighbours[2]))
				{
					case 0:

					nclusters_set_value(nclusters,x,y,l,id);
					labels[id]=id;
					id++;
					assert(id<MAX_NR_OF_CLUSTERS);

					break;

					case 1:

					nclusters_set_value(nclusters,x,y,l,max3(neighbours[0],neighbours[1],neighbours[2]));

					break;

					case 2:
					{
						int first,second;

						if(neighbours[0]==0)
						{
							first=neighbours[1];
							second=neighbours[2];
						}
						else if(neighbours[1]==0)
						{
							first=neighbours[0];
							second=neighbours[2];
						}
						else
						{
							first=neighbours[0];
							second=neighbours[1];
						}

						nclusters_set_value(nclusters,x,y,l,hk_union(labels,first,second));
					}

					break;

					case 3:

					nclusters_set_value(nclusters,x,y,l,hk_union(labels,neighbours[0],hk_union(labels,neighbours[1],neighbours[2])));

					break;

					default:
					assert(false);
				}
			}
		}
	}

	for(int x=0;x<nclusters->lx;x++)
	{
		for(int y=0;y<nclusters->ly;y++)
		{
			for(int l=0;l<nclusters->nrlayers;l++)
			{
				int value=nclusters_get_value(nclusters,x,y,l);

				nclusters_set_value(nclusters,x,y,l,labels[value]);
			}
		}
	}

	id=1;
	for(int c=0;c<MAX_NR_OF_CLUSTERS;c++)
		if(labels[c]==c)
			id++;

	return id;
}

int nclusters_identify(struct nclusters_t *nclusters)
{
	int id=1;

	assert(nclusters);

	for(int x=0;x<nclusters->lx;x++)
		for(int y=0;y<nclusters->ly;y++)
			for(int l=0;l<nclusters->nrlayers;l++)
				nclusters_set_value(nclusters,x,y,l,0);

	for(int x=0;x<nclusters->lx;x++)
	{
		for(int y=0;y<nclusters->ly;y++)
		{
			for(int l=0;l<nclusters->nrlayers;l++)
			{
				if(nclusters_get_value(nclusters,x,y,l)==0)
				{
					nclusters_grow(nclusters,x,y,l,id);

					id++;
				}
			}
		}
	}

	return id;
}

void nclusters_reset(struct nlayer_t *nl,struct nclusters_t *nclusters,int target,int value)
{
	assert(nl);
	assert(nclusters);

	for(int x=0;x<nclusters->lx;x++)
		for(int y=0;y<nclusters->ly;y++)
			for(int l=0;l<nl->nrlayers;l++)
				if(nclusters_get_value(nclusters,x,y,l)==target)
					ising2d_set_spin(nl->layers[l],x,y,value);

}
