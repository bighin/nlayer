#ifndef __NLAYER_H__
#define __NLAYER_H__

#include "config.h"
#include "ising.h"

/*
	This struct identifies a n-layer, two-dimensional Ising-like configuration.

*/

struct nlayer_t
{
	struct ising2d_t *layers[MAX_NR_OF_LAYERS];

	int lx,ly,nrlayers;
	double Js[MAX_NR_OF_LAYERS],Jperps[MAX_NR_OF_LAYERS],beta;
};

struct nlayer_t *nlayer_init(int x,int y,int nrlayers,const double Js[MAX_NR_OF_LAYERS],const double Jperps[MAX_NR_OF_LAYERS],double beta);
void nlayer_fini(struct nlayer_t *nl);

int nlayer_get_spin(struct nlayer_t *nl,int x,int y,int l);
void nlayer_set_spin(struct nlayer_t *nl,int x,int y,int l,int spin);
void nlayer_flip_spin(struct nlayer_t *nl,int x,int y,int l);

double nlayer_observable(struct nlayer_t *nl,double (*ising_observable)(struct ising2d_t *));
double nlayer_magnetization(struct nlayer_t *nl);

#endif //__NLAYER_H__
