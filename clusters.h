#ifndef __CLUSTERS_H__
#define __CLUSTERS_H__

#include "ising.h"
#include "nlayer.h"

struct nclusters_t
{
	int *vals[MAX_NR_OF_LAYERS];
	int lx,ly,nrlayers;

	struct ibond2d_t *bonds[MAX_NR_OF_LAYERS];
	struct ivbond2d_t *ivbonds[MAX_NR_OF_LAYERS-1];
};

struct nclusters_t *nclusters_init(int x,int y,int nrlayers);
void nclusters_fini(struct nclusters_t *bc);
int nclusters_get_value(struct nclusters_t *nclusters,int x,int y,int layer);
void nclusters_set_value(struct nclusters_t *nclusters,int x,int y,int layer,int value);
int nclusters_grow(struct nclusters_t *nclusters,int x,int y,int l,int nr_clusters);
int nclusters_identify(struct nclusters_t *nclusters);
void nclusters_reset(struct nlayer_t *nl,struct nclusters_t *nclusters,int target,int value);

struct stack_t
{
	int *data;
	int size,alloced;
};

struct stack_t *stack_init(void);
void stack_push(struct stack_t *stack,int x,int y,int l);
void stack_pop(struct stack_t *stack,int *x,int *y,int *l);
void stack_fini(struct stack_t *stack);

#endif
