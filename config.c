#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/time.h>
#include <gsl/gsl_rng.h>

#include "config.h"
#include "auxx.h"
#include "mc.h"
#include "inih/ini.h"

void load_configuration_defaults(struct configuration_t *config)
{
	config->x=config->y=config->nrlayers=-1;
	config->model=MODEL_UNDEFINED;
	config->nrJs=config->nrJperps=0;
	config->beta=-1.0f;

	config->runs=config->thermalization=config->sampling=config->decorrelation=-1;
	config->prefix=NULL;

	config->savesnapshots=true;
	config->updates=-1;

	config->verbose=false;
}

int configuration_handler(void *user,const char *section,const char *name,const char *value)
{
	struct configuration_t *config=(struct configuration_t *)(user);

#define MATCH(s,n) (strcmp(section,s)==0)&&(strcmp(name,n)==0)

	if(MATCH("general","model"))
	{
		if(strcmp(value,"ising")==0)
			config->model=MODEL_ISING_NLAYER;
		else if(strcmp(value,"ashkinteller")==0)
			config->model=MODEL_ASHKIN_TELLER;
	}
	else if(MATCH("general","x"))
	{
		config->x=atoi(value);
	}
	else if(MATCH("general","y"))
	{
		config->y=atoi(value);
	}
	else if(MATCH("general","nrlayers"))
	{
		config->nrlayers=atoi(value);
	}
	else if(MATCH("coupling","J"))
	{
		char *token,*string,*tofree;

		config->nrJs=0;		
		tofree=string=strdup(value);

		while((token=strsep(&string,","))!=NULL)
		{
			double thisvalue=atof(token);

			config->Js[config->nrJs]=thisvalue;
			config->nrJs++;
		}

		free(tofree);
	}
	else if(MATCH("coupling","Jperp"))
	{
		char *token,*string,*tofree;

		config->nrJperps=0;		
		tofree=string=strdup(value);

		while((token=strsep(&string,","))!=NULL)
		{
			double thisvalue=atof(token);

			config->Jperps[config->nrJperps]=thisvalue;
			config->nrJperps++;
		}

		free(tofree);
	}
	else if(MATCH("coupling","beta"))
	{
		config->beta=atof(value);
	}
	else if(MATCH("mc","runs"))
	{
		config->runs=atoi(value);
	}
	else if(MATCH("mc","thermalization"))
	{
		config->thermalization=atoi(value);
	}
	else if(MATCH("mc","sampling"))
	{
		config->sampling=atoi(value);
	}
	else if(MATCH("mc","decorrelation"))
	{
		config->decorrelation=atoi(value);
	}
	else if(MATCH("mc","updates"))
	{
		if(strcmp(value,"sw")==0)
			config->updates=UPDATES_SW;
		else if(strcmp(value,"wolff")==0)
			config->updates=UPDATES_WOLFF;
	}
	else if(MATCH("output","prefix"))
	{
		if(strcmp(value,"auto")==0)
		{
			if(!strstr(config->inipath,".ini"))
			{
				printf("Error: using automatic prefix, but the configuration file path does not contain '.ini'\n");
				exit(0);
			}

			config->prefix=find_and_replace(config->inipath,".ini","");
		}
		else
		{
			config->prefix=strdup(value);
		}
	}
	else if(MATCH("output","savesnapshots"))
	{
		if(!strcasecmp(value,"true"))
			config->savesnapshots=true;
		else
			config->savesnapshots=false;
	}
	else
	{
		return 0;  /* unknown section/name, error */
	}

	return 1;
}

bool check_configuration(struct configuration_t *config)
{
	if(config->model==MODEL_UNDEFINED)
	{
		printf("Please specify a model (either Ising n-layer or Ashkin-Teller) to simulate.\n");
		return false;
	}

	if((config->model==MODEL_ASHKIN_TELLER)&&(config->nrJperps!=1))
	{
		printf("For the Ashkin-Teller model you must specify exactly one Jperp.\n");
		return false;
	}

	if((config->model==MODEL_ASHKIN_TELLER)&&(!((config->nrJs==1)||(config->nrJs==2))))
	{
		printf("For the Ashkin-Teller model you must specify one or two values of the coupling J.\n");
		return false;
	}

	if((config->x==-1)||(config->y==-1)||(config->nrlayers==-1))
	{
		printf("Please specify the x and y dimensions of the lattice, as well as the number of layers.\n");
		printf("%d %d %d\n",config->x,config->y,config->nrlayers);
		return false;
	}

	if((config->x<1)||(config->x>MAX_LATTICE_SIZE))
	{
		printf("The lattice size in either direction must be between 1 and %d.\n",MAX_LATTICE_SIZE);
		return false;
	}

	if((config->y<1)||(config->y>65536))
	{
		printf("The lattice size in either direction must be between 1 and %d.\n",MAX_LATTICE_SIZE);
		return false;
	}

	if((config->nrlayers<1)||(config->nrlayers>MAX_NR_OF_LAYERS))
	{
		printf("The number of layers must be between 1 and %d.\n",MAX_NR_OF_LAYERS);
		return false;
	}

	if(config->beta<=0.0f)
	{
		printf("You must specify a non-negative beta value.\n");
		return false;
	}

	if(!((config->nrJs==1)||(config->nrJs==config->nrlayers)))
	{
		printf("You must either specify one single J (for every layer), or one J per layer.\n");
		return false;
	}

	if(!((config->nrJperps==1)||(config->nrJperps==(config->nrlayers-1))))
	{
		printf("You must either specify one single Jperp (for every intra-layer coupling), or (n-1) Jperps, where n is the number of layers.\n");
		return false;
	}
	
	if((config->runs==-1)||(config->thermalization==-1)||(config->sampling==-1)||(config->decorrelation==-1))
	{
		printf("Please specify the number of runs, thermalization steps, samplings steps, decorrelation steps.\n");
		return false;
	}

	if(config->runs<1)
	{
		printf("The number of Monte Carlo runs cannot be less than 1.");
		return false;
	}

	if(config->thermalization<1)
	{
		printf("The number of thermalization steps cannot be less than 1.");
		return false;
	}

	if(config->sampling<1)
	{
		printf("The number of sampling steps cannot be less than 1.");
		return false;
	}

	if(config->decorrelation<1)
	{
		printf("The number of decorrelation steps cannot be less than 1.");
		return false;
	}

	if(config->updates==-1)
	{
		printf("You must specify a valid update type (either 'sw' or 'wolff').");
		return false;
	}

	if(config->prefix==NULL)
	{
		printf("Please specify an output prefix.\n");
		return false;
	}
	
	return true;
}

void show_configuration_summary(struct configuration_t *config,char *inifile)
{
	printf("nLayer Ising Monte Carlo.\n");
	printf("Loaded configuration from: %s\n\n",inifile);

	printf("Lattice:\n");
	printf("\tDimensions: (%d,%d)\n",config->x,config->y);
	printf("\tLayers: %d\n\n",config->nrlayers);

	printf("Couplings:\n");
	printf("\tbeta: %f\n",config->beta);
	printf("\tJ: %f",config->Js[0]);
	
	for(int c=1;c<config->nrJs;c++)
		printf(", %f",config->Js[c]);
	
	printf("\n\tJperp: %f",config->Jperps[0]);

	for(int c=1;c<config->nrJperps;c++)
		printf(", %f",config->Jperps[c]);

	printf("\n\nMonte Carlo simulation:\n");
	printf("\tNumber of runs: %d\n",config->runs);
	printf("\tThermalization steps: %d\n",config->thermalization);
	printf("\tSampling steps: %d\n",config->sampling);
	printf("\tDecorrelation steps: %d\n",config->decorrelation);

	if(config->updates==UPDATES_SW)
		printf("\tUpdates algorithm: Swendsen-Wang\n\n");
	else if(config->updates==UPDATES_WOLFF)
		printf("\tUpdates algorithm: Wolff\n\n");

	printf("Output prefix: %s\n",config->prefix);
	printf("(individual snapshots will %sbe saved)\n\n",(config->savesnapshots==false)?("NOT "):(""));
}

void save_ini_backup(struct configuration_t *config,char *inifile)
{
	FILE *in,*out;
	char fname[1024];
	
	snprintf(fname,1024,"%s.inibackup.dat",config->prefix);
	fname[1023]='\0';

	if(!(in=fopen(inifile,"r")))
		return;

	if(!(out=fopen(fname,"w+")))
	{
		if(in)
			fclose(in);
	
		return;
	}

	fprintf(out,"; Binary compiled from commit: %s\n",GITCOMMIT);
	fcopy(in,out);

	if(in)
		fclose(in);

	if(out)
		fclose(out);
}

bool do_ini_file(char *inifile)
{
	struct configuration_t config;
	gsl_rng *rng_ctx;
	struct timeval starttime,endtime;
	double elapsed_time;

	load_configuration_defaults(&config);
	config.inipath=inifile;

	if(ini_parse(inifile,configuration_handler,&config)<0)
	{
		printf("Can't load '%s'\n",inifile);
		return false;
	}

	if(check_configuration(&config)!=true)
		return false;

	if(config.nrJs==1)
	{
		for(int c=1;c<config.nrlayers;c++)
			config.Js[c]=config.Js[0];
	}

	if(config.nrJperps==1)
	{
		for(int c=1;c<(config.nrlayers-1);c++)
			config.Jperps[c]=config.Jperps[0];
	}

	show_configuration_summary(&config,inifile);
	save_ini_backup(&config,inifile);

	/*
		We initialize the random number generator, and we seed it in case a NON deterministic seed is requested.
	*/

	rng_ctx=gsl_rng_alloc(gsl_rng_mt19937);
	assert(rng_ctx!=NULL);
	seed_rng(rng_ctx);

	/*
		Time to actually do things!
	*/

	gettimeofday(&starttime,NULL);
	do_mc(&config,rng_ctx,stdout,true);
	gettimeofday(&endtime,NULL);

	if(config.verbose)
	{
		elapsed_time=(endtime.tv_sec-starttime.tv_sec)*1000.0;
		elapsed_time+=(endtime.tv_usec-starttime.tv_usec)/1000.0;
		elapsed_time/=1000;

		printf("Elapsed time: %f s\n",elapsed_time);
	}

	return true;
}
