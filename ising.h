#ifndef __ISING_H__
#define __ISING_H__

#include "config.h"

#define MAKE_INDEX(ctx,x,y)	((x)+ctx->lx*(y))

/*
	This struct identifies a two-dimensional Ising-like configuration.
*/

struct ising2d_t
{
	int *spins;
	int lx,ly;
};

struct ising2d_t *ising2d_init(int x,int y);
void ising2d_fini(struct ising2d_t *s);
int ising2d_get_spin(struct ising2d_t *s,int x,int y);
void ising2d_set_spin(struct ising2d_t *s,int x,int y,int spin);

double ising2d_magnetization(struct ising2d_t *s);

double st_magnetization(struct ising2d_t *sigma, struct ising2d_t *tau);
double stu_magnetization(struct ising2d_t *sigma, struct ising2d_t *tau, struct ising2d_t *upsilon);


#endif //__ISING_H__
