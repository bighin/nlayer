#include <assert.h>
#include <stdlib.h>

#include "ising.h"

struct ising2d_t *ising2d_init(int x,int y)
{
	struct ising2d_t *ret;

	assert(x>0);
	assert(y>0);

	if(!(ret=malloc(sizeof(struct ising2d_t))))
		return NULL;
	
	ret->spins=malloc(sizeof(int)*x*y);

	if(!ret->spins)
	{
		if(ret)
			free(ret);
		
		return NULL;
	}
	
	ret->lx=x;
	ret->ly=y;
	
	return ret;
}

void ising2d_fini(struct ising2d_t *s)
{
	if(s)
	{
		if(s->spins)
			free(s->spins);
		
		free(s);
	}
}

int ising2d_get_spin(struct ising2d_t *s,int x,int y)
{
	assert(s!=NULL);
	assert((x>=0)&&(x<s->lx));
	assert((y>=0)&&(y<s->ly));

	return s->spins[MAKE_INDEX(s,x,y)];
}

void ising2d_set_spin(struct ising2d_t *s,int x,int y,int spin)
{
	assert(s!=NULL);
	assert((x>=0)&&(x<s->lx));
	assert((y>=0)&&(y<s->ly));

	s->spins[MAKE_INDEX(s,x,y)]=spin;
}

double ising2d_magnetization(struct ising2d_t *s)
{
	double ret=0.0f;

	for(int x=0;x<s->lx;x++)
		for(int y=0;y<s->ly;y++)
			ret+=s->spins[MAKE_INDEX(s,x,y)];

	return ret/(s->lx*s->ly);
}

double st_magnetization(struct ising2d_t *sigma, struct ising2d_t *tau)
{
	double ret=0.0f;

	assert(sigma->lx==tau->lx);
	assert(sigma->ly==tau->ly);

	for(int x=0;x<sigma->lx;x++)
		for(int y=0;y<sigma->ly;y++)
			ret+=sigma->spins[MAKE_INDEX(sigma,x,y)]*tau->spins[MAKE_INDEX(tau,x,y)];

	return ret/(sigma->lx*sigma->ly);
}

double stu_magnetization(struct ising2d_t *sigma, struct ising2d_t *tau, struct ising2d_t *upsilon)
{
	double ret=0.0f;

	assert(sigma->lx==tau->lx);
	assert(sigma->ly==tau->ly);

	assert(sigma->lx==upsilon->lx);
	assert(sigma->ly==upsilon->ly);


	for(int x=0;x<sigma->lx;x++)
		for(int y=0;y<sigma->ly;y++)
			ret+=sigma->spins[MAKE_INDEX(sigma,x,y)]*tau->spins[MAKE_INDEX(tau,x,y)]*upsilon->spins[MAKE_INDEX(upsilon,x,y)];

	return ret/(sigma->lx*sigma->ly);
}
