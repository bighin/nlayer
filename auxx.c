#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <gsl/gsl_rng.h>

#include "auxx.h"

void fcopy(FILE *f1,FILE *f2)
{
	char buffer[1024];
	size_t n;

	while((n=fread(buffer,sizeof(char),1024,f1))>0)
	{
		if (fwrite(buffer,sizeof(char),n,f2)!=n)
			perror("Write failed!\n");
	}
}

/*
 * From: https://stackoverflow.com/questions/4833347/removing-substring-from-a-string
 * 
 * Description:
 *   Find and replace text within a string.
 *
 * Parameters:
 *   src  (in) - pointer to source string
 *   from (in) - pointer to search text
 *   to   (in) - pointer to replacement text
 *
 * Returns:
 *   Returns a pointer to dynamically-allocated memory containing string
 *   with occurences of the text pointed to by 'from' replaced by with the
 *   text pointed to by 'to'.
 */

char *find_and_replace(const char *src,const char *from,const char *to)
{
	/*
	 * Find out the lengths of the source string, text to replace, and
	 * the replacement text.
	 */

	size_t size=1+strlen(src);
	size_t fromlen=strlen(from);
	size_t tolen=strlen(to);

	/*
	 * Allocate the first chunk with enough for the original string.
	 */

	char *value=malloc(size);

	/*
	 * We need to return 'value', so let's make a copy to mess around with.
	 */

	char *dst=value;

	/*
	 * Before we begin, let's see if malloc was successful.
	 */

	if(value!=NULL)
	{
		/*
		 * Loop until no matches are found.
		 */

		for(;;)
		{
			/*
			 * Try to find the search text.
			 */

			const char *match=strstr(src,from);
			if(match!=NULL)
			{
				/*
				 * Found search text at location 'match'. :)
				 * Find out how many characters to copy up to the 'match'.
				 */

				size_t count=match-src;

				/*
				 * We are going to realloc, and for that we will need a
				 * temporary pointer for safe usage.
				 */

				char *temp;

				/*
				 * Calculate the total size the string will be after the
				 * replacement is performed.
				 */

				size+=tolen-fromlen;

				/*
				 * Attempt to realloc memory for the new size.
				 */

				temp=realloc(value,size);
				if(temp==NULL)
				{
					/*
					 * Attempt to realloc failed. Free the previously malloc'd
					 * memory and return with our tail between our legs. :(
					 */

					free(value);
					return NULL;
				}

				/*
				 * The call to realloc was successful. :) But we'll want to
				 * return 'value' eventually, so let's point it to the memory
				 * that we are now working with. And let's not forget to point
				 * to the right location in the destination as well.
				 */

				dst=temp+(dst-value);
				value=temp;

				/*
				 * Copy from the source to the point where we matched. Then
				 * move the source pointer ahead by the amount we copied. And
				 * move the destination pointer ahead by the same amount.
				 */

				memmove(dst,src,count);
				src+=count;
				dst+=count;

				/*
				 * Now copy in the replacement text 'to' at the position of
				 * the match. Adjust the source pointer by the text we replaced.
				 * Adjust the destination pointer by the amount of replacement
				 * text.
				 */

				memmove(dst,to,tolen);
				src+=fromlen;
				dst+=tolen;
			}
			else
			{
				/* No match found. */

				/*
				 * Copy any remaining part of the string. This includes the null
				 * termination character.
				 */

				strcpy(dst,src);
				break;
		        }
		}
	}

	return value;
}

/*
	Routine to seed GSL's random number generator
*/

void seed_rng(gsl_rng *rng)
{
	char *devname="/dev/urandom";
	FILE *dev;
        unsigned long seed;

	if((dev=fopen(devname,"r"))!=NULL)
	{
		fread(&seed,sizeof(unsigned long),1,dev);
		fclose(dev);

		gsl_rng_set(rng,seed);
	}
	else
	{
		printf("Warning: couldn't read from %s to seed the RNG.\n",devname);
	}
}

/*
	Quick routine for double comparison
*/

bool almost_same_double(double a,double b)
{
	if((fabs(a)<10e-7)&&(fabs(b)<10e-7))
		return true;

	if((fabs(a-b)/fabs(a))<10e-7)
		return true;

	return false;
}
