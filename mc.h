#ifndef __MC_H__
#define __MC_H__

#include <stdio.h>
#include <math.h>
#include <gsl/gsl_rng.h>

#include "config.h"
#include "nlayer.h"
#include "stat.h"

bool validate_jpermutations(void);

void do_mc(struct configuration_t *config,gsl_rng *rng_ctx,FILE *out,bool showheader);

#endif //__MC_H__
