# Opportunities for speeding up the code

- Clusters should be identified using the Hoshen-Kopelman, rather than the current flood-fill algorithm.
Some preliminary (potentially buggy) code is already in place in clusters.c, but not active.

- When doing Wolff updates (both for Ising and Ashkin-Teller) we create all the bonds, and then we select a random spin
and grow a cluster from there. It would be much faster to create the bonds 'on the fly', as the cluster expands.
A 'real' Wolff code has been written (see actual_wolff_ashkin_teller_step() and actual_wolff_ising_step() in updates.c
but has to be activated and tested.)

# Things to check

- Check the Wolff dynamics for Ashkin-Teller. For instance they could be validated against Swendsen-Wang.

# Improvements

- Probably the code could be extended to anti-ferromagnetic couplings (both for Ising and Ashkin-Teller)
