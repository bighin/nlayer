#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <gsl/gsl_rng.h>

#include "nlayer.h"
#include "clusters.h"
#include "bonds.h"
#include "stat.h"
#include "auxx.h"
#include "updates.h"
#include "mc.h"

void save_snapshot(char *prefix,int count,struct nlayer_t *nl)
{
	char fname[1024];
	FILE *out;

	if(prefix==NULL)
		return;

	snprintf(fname,1024,"%s.snap%d.dat",prefix,count);
	fname[1023]='\0';

	if(!(out=fopen(fname,"w+")))
	{
		printf("Couldn't open %s for writing.\n",fname);
		return;
	}

	for(int l=0;l<nl->nrlayers;l++)
	{
		for(int x=0;x<nl->lx;x++)
		{
			for(int y=0;y<nl->ly;y++)
			{
				int spin=nlayer_get_spin(nl,x,y,l);

				assert((spin==1)||(spin==-1));

				fprintf(out, "%c",(spin==1)?('+'):('-'));
			}

			fprintf(out,"\n");
		}

		fprintf(out,"\n");
	}

	if(out)
		fclose(out);
}

/*
	For Ashkin-Teller we might need to use a permutation of the couplings,
	see e.g. hep-lat/9511022, Section 2 and 3. The permutation symmetries
	are also analyzed in Baxter's book.
*/

#define NR_OF_J_PERMUTATIONS	(6)

const int jpermutations[NR_OF_J_PERMUTATIONS][3]=
{
	{0,1,2},
	{0,2,1},
	{1,0,2},
	{1,2,0},
	{2,0,1},
	{2,1,0}
};

const int invjpermutations[NR_OF_J_PERMUTATIONS][3]=
{
	{0,1,2},
	{0,2,1},
	{1,0,2},
	{2,0,1},
	{1,2,0},
	{2,1,0}
};

bool validate_jpermutations(void)
{
	for(int c=0;c<NR_OF_J_PERMUTATIONS;c++)
	{
		if(invjpermutations[c][jpermutations[c][0]]!=0)
			return false;

		if(invjpermutations[c][jpermutations[c][1]]!=1)
			return false;

		if(invjpermutations[c][jpermutations[c][2]]!=2)
			return false;
	}

	for(int c=0;c<NR_OF_J_PERMUTATIONS;c++)
	{
		if(jpermutations[c][invjpermutations[c][0]]!=0)
			return false;

		if(jpermutations[c][invjpermutations[c][1]]!=1)
			return false;

		if(jpermutations[c][invjpermutations[c][2]]!=2)
			return false;
	}

	return true;
}

void save_snapshot_with_permutations(char *prefix,int count,struct nlayer_t *nl,int jpermutation)
{
	char fname[1024];
	FILE *out;

	assert(jpermutation>=0);
	assert(jpermutation<NR_OF_J_PERMUTATIONS);

	assert(nl->nrlayers==2);

	if(prefix==NULL)
		return;

	snprintf(fname,1024,"%s.snap%d.dat",prefix,count);
	fname[1023]='\0';

	if(!(out=fopen(fname,"w+")))
	{
		printf("Couldn't open %s for writing.\n",fname);
		return;
	}

	for(int l=0;l<2;l++)
	{
		for(int x=0;x<nl->lx;x++)
		{
			for(int y=0;y<nl->ly;y++)
			{
				int spins[3];

				spins[0]=nlayer_get_spin(nl,x,y,0);
				spins[1]=nlayer_get_spin(nl,x,y,1);
				spins[2]=nlayer_get_spin(nl,x,y,0)*nlayer_get_spin(nl,x,y,1);

				fprintf(out,"%c",(spins[invjpermutations[jpermutation][l]]==1)?('+'):('-'));
			}

			fprintf(out,"\n");
		}

		fprintf(out,"\n");
	}

	if(out)
		fclose(out);
}

bool ashkin_teller_find_jpermutation(struct nlayer_t *nl,int *jpermutation)
{
	double couplings[3];

	couplings[0]=nl->Js[0];
	couplings[1]=nl->Js[1];
	couplings[2]=nl->Jperps[0];

	for(int c=0;c<NR_OF_J_PERMUTATIONS;c++)
	{
		if((couplings[jpermutations[c][0]]>=fabs(couplings[jpermutations[c][2]]))&&
			(couplings[jpermutations[c][1]]>=fabs(couplings[jpermutations[c][2]])))
		{
			*jpermutation=c;

			assert(*jpermutation>=0);
			assert(*jpermutation<NR_OF_J_PERMUTATIONS);

			nl->Js[0]=couplings[jpermutations[*jpermutation][0]];
			nl->Js[1]=couplings[jpermutations[*jpermutation][1]];
			nl->Jperps[0]=couplings[jpermutations[*jpermutation][2]];

			assert(nl->Js[0]>=fabs(nl->Jperps[0]));
			assert(nl->Js[1]>=fabs(nl->Jperps[0]));

			return true;
		}
	}

	return false;
}

const char *channel_ids[]=
{
	"m","m2","|m|",
	"s","s2","|s|",
	"t","t2","|t|",
	"st","(st)^2","|st|",
	"u","u2","|u|",
	"su","(su)^2","|su|",
	"tu","(tu)^2","|tu|",
	"stu","(stu)^2","|stu|",
	"e","e^2",
	NULL
};

int get_channel_nr(const char *id)
{
	for(int c=0;channel_ids[c]!=NULL;c++)
		if(strcmp(channel_ids[c],id)==0)
			return c;

	assert(false);

	return 0;
}

int get_total_channels(void)
{
	int c;

	for(c=0;channel_ids[c]!=NULL;c++)
		/* nothing */;

	assert(channel_ids[c]==NULL);

	return c;
}

void ising_collect_samples(struct nlayer_t *nl,struct sampling_ctx_t *sctx)
{
	double magnetization=nlayer_magnetization(nl);

	sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("m"),magnetization);
	sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("m2"),magnetization*magnetization);
	sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("|m|"),fabs(magnetization));

	if(nl->nrlayers>=2)
	{
		double s,t,st;

		s=ising2d_magnetization(nl->layers[0]);
		t=ising2d_magnetization(nl->layers[1]);
		st=st_magnetization(nl->layers[0],nl->layers[1]);

		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("s"),s);
		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("s2"),s*s);
		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("|s|"),fabs(s));

		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("t"),t);
		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("t2"),t*t);
		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("|t|"),fabs(t));

		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("st"),st);
		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("(st)^2"),st*st);
		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("|st|"),fabs(st));
	}

	if(nl->nrlayers>=3)
	{
		double u,su,tu,stu;

		u=ising2d_magnetization(nl->layers[2]);
		su=st_magnetization(nl->layers[0],nl->layers[2]);
		tu=st_magnetization(nl->layers[1],nl->layers[2]);
		stu=stu_magnetization(nl->layers[0],nl->layers[1],nl->layers[2]);

		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("u"),u);
		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("u2"),u*u);
		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("|u|"),fabs(u));

		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("su"),su);
		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("(su)^2"),su*su);
		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("|su|"),fabs(su));

		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("tu"),tu);
		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("(tu)^2"),tu*tu);
		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("|tu|"),fabs(tu));

		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("stu"),stu);
		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("(stu)^2"),stu*stu);
		sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("|stu|"),fabs(stu));
	}
}

double ashkin_teller_energy_per_bond(struct nlayer_t *nl,int x,int y,int xprime,int yprime)
{
	double s,t,sprime,tprime;

	/*
		Note that no permutations are needed here, since the energy
		is invariant for permutations.
	*/

	s=nlayer_get_spin(nl,x,y,0);
	t=nlayer_get_spin(nl,x,y,1);

	sprime=nlayer_get_spin(nl,xprime,yprime,0);
	tprime=nlayer_get_spin(nl,xprime,yprime,1);

	return -nl->Js[0]*s*sprime-nl->Js[1]*t*tprime-nl->Jperps[0]*s*sprime*t*tprime;
}

double ashkin_teller_energy(struct nlayer_t *nl)
{
	double result=0.0f;

	for(int x=0;x<nl->lx;x++)
	{
		for(int y=0;y<nl->ly;y++)
		{
			result+=ashkin_teller_energy_per_bond(nl, x, y, (x+1)%nl->lx, y);
			result+=ashkin_teller_energy_per_bond(nl, x, y, x, (y+1)%nl->ly);
		}
	}

	return result;
}

void ashkin_teller_collect_samples(struct nlayer_t *nl,int jpermutation,struct sampling_ctx_t *sctx)
{
	double s,t,st;
	double observables[3];

	s=ising2d_magnetization(nl->layers[0]);
	t=ising2d_magnetization(nl->layers[1]);
	st=st_magnetization(nl->layers[0], nl->layers[1]);

	observables[0]=s;
	observables[1]=t;
	observables[2]=st;

	s=observables[invjpermutations[jpermutation][0]];
	t=observables[invjpermutations[jpermutation][1]];
	st=observables[invjpermutations[jpermutation][2]];

	sampling_ctx_add_entry_to_channel(sctx, get_channel_nr("s"),s);
	sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("s2"),s*s);
	sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("|s|"),fabs(s));

	sampling_ctx_add_entry_to_channel(sctx, get_channel_nr("t"),t);
	sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("t2"),t*t);
	sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("|t|"),fabs(t));

	sampling_ctx_add_entry_to_channel(sctx, get_channel_nr("st"),st);
	sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("(st)^2"),st*st);
	sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("|st|"),fabs(st));

	double energy=ashkin_teller_energy(nl);

	sampling_ctx_add_entry_to_channel(sctx, get_channel_nr("e"),energy);
	sampling_ctx_add_entry_to_channel(sctx,get_channel_nr("e^2"),energy*energy);
}

typedef int (updates_function_t)(struct nlayer_t *nl,gsl_rng *rng_ctx);

updates_function_t *get_updates_function(struct configuration_t *config)
{
	int (*updates_function)(struct nlayer_t *nl,gsl_rng *rng_ctx)=NULL;

	switch(config->model)
	{
		case MODEL_ISING_NLAYER:
		{
			switch(config->updates)
			{
				case UPDATES_SW:
				updates_function=sw_nlayer_step;
				break;

				case UPDATES_WOLFF:
				updates_function=wolff_nlayer_step;
				break;
			}
		}
			break;

		case MODEL_ASHKIN_TELLER:
		{
			switch(config->updates)
			{
				case UPDATES_SW:
				updates_function=sw_ashkin_teller_step;
				break;

				case UPDATES_WOLFF:
				updates_function=wolff_ashkin_teller_step;
				break;
			}
		}
			break;
	}

	assert(updates_function!=NULL);

	return updates_function;
}

struct sampling_ctx_t *mc_single_run(struct configuration_t *config,gsl_rng *rng_ctx,bool writesnapshots)
{
	struct nlayer_t *nl;
	struct sampling_ctx_t *sctx;

	int (*updates_function)(struct nlayer_t *nl,gsl_rng *rng_ctx);

	if((config->model==MODEL_ASHKIN_TELLER)&&(config->nrlayers!=2))
	{
		printf("Simulation failed: you must use two layers when simulating an Ashkin-Teller model.\n");
		return NULL;
	}

	nl=nlayer_init(config->x,config->y,config->nrlayers,config->Js,config->Jperps,config->beta);

	/*
		The algorithms that we use for Ashkin-Teller can only simulate a certain
		range of couplings, i.e. J,J' >= |K|.

		This means that we must use the symmetry properties of the Ashkin-Teller model
		to arrange the couplings in a suitable way.
	*/

	int jpermutation=0;

	if(config->model==MODEL_ASHKIN_TELLER)
	{
		if(ashkin_teller_find_jpermutation(nl,&jpermutation)==false)
		{
			printf("Couldn't find a suitable permutation of couplings for Ashkin-Teller simulation. Quitting.\n");
			nlayer_fini(nl);

			assert(false);
			exit(0);
		}
	}

	updates_function=get_updates_function(config);

	/*
		Now we can start setting up the structures needed for sampling,
		initializing the lattice, etc...
	*/

	sctx=sampling_ctx_init(get_total_channels());

	for(int x=0;x<nl->lx;x++)
		for(int y=0;y<nl->ly;y++)
			for(int l=0;l<nl->nrlayers;l++)
				nlayer_set_spin(nl,x,y,l,2*gsl_rng_uniform_int(rng_ctx,2)-1);

	/*
		Actual simulations start here.

		Note that one MC step corresponds to flipping a number of spins equal
		to the lattice dimensions.

		For the thermalization part we wait config->thermalization MC steps,
		whereas the sampling lasts config->sampling MC steps. During this time,
		every config->decorrelation MC steps (optionally) a snapshot is taken.
	*/

	int spins_on_lattice=nl->lx*nl->ly*nl->nrlayers;
	int flipped_spins=0;
	int mcsteps=0;
	int written_snapshots=0;

	while(flipped_spins<config->thermalization*spins_on_lattice)
	{
		flipped_spins+=updates_function(nl,rng_ctx);

		if(config->verbose==true)
			printf("Thermalisation: %d/%d\n",flipped_spins,config->thermalization*spins_on_lattice);
	}

	flipped_spins=0;
	mcsteps=0;

	while(mcsteps<config->sampling)
	{
		/*
			Monte Carlo steps
		*/

		flipped_spins+=updates_function(nl,rng_ctx);

		/*
			Sampling/spin snapshots
		*/

		if(flipped_spins>=spins_on_lattice)
		{
			flipped_spins-=spins_on_lattice;
			mcsteps++;

			if((mcsteps%config->decorrelation)==0)
			{
				switch(config->model)
				{
					case MODEL_ISING_NLAYER:
					ising_collect_samples(nl,sctx);

					if(writesnapshots==true)
						save_snapshot(config->prefix,written_snapshots++,nl);

					break;

					case MODEL_ASHKIN_TELLER:
					ashkin_teller_collect_samples(nl,jpermutation,sctx);

					if(writesnapshots==true)
						save_snapshot_with_permutations(config->prefix,written_snapshots++,nl,jpermutation);

					break;

					default:
					assert(false);
				}
			}

			if(config->verbose==true)
				printf("Sampling: %d/%d\n",mcsteps,config->sampling);
		}
	}

	nlayer_fini(nl);

	return sctx;
}

const struct ising_channel_ids_t
{
	int minlayers;
	char *id;
}
ising_channel_ids[]=
{
	/*
		Note that the order here determines the order in the output files:
		we first output some 'primary' variables, for conveniency's sake...
	*/

	{1,"m"},
	{2,"s"},
	{2,"t"},
	{2,"st"},
	{3,"u"},
	{3,"su"},
	{3,"tu"},
	{3,"stu"},

	/*
		...and then some 'derived' quantities.
	*/

	{1,"m2"},
	{1,"|m|"},
	{2,"s2"},
	{2,"|s|"},
	{2,"t2"},
	{2,"|t|"},
	{2,"(st)^2"},
	{2,"|st|"},
	{3,"u2"},
	{3,"|u|"},
	{3,"(su)^2"},
	{3,"|su|"},
	{3,"(tu)^2"},
	{3,"|tu|"},
	{3,"(stu)^2"},
	{3,"|stu|"},
	{0,NULL}
};

/*
	Note that the order here determines the order in the output files
*/

const char *ashkin_teller_channel_ids[]=
{
	"s","t","st",
	"s2","|s|",
	"t2","|t|",
	"(st)^2","|st|",
	"e","e^2",
	NULL
};

void print_header(struct configuration_t *config,FILE *out)
{
	switch(config->model)
	{
		case MODEL_ISING_NLAYER:
		{
			fprintf(out, "# <beta> <J1> <J2> <K> ");

			for(int c=0;ising_channel_ids[c].id!=NULL;c++)
				if(config->nrlayers>=ising_channel_ids[c].minlayers)
					fprintf(out, "<%s> ", ising_channel_ids[c].id);

			fprintf(out,"\n");
		}
		break;

		case MODEL_ASHKIN_TELLER:
		{
			fprintf(out, "# <beta> <J1> <J2> <K> ");

			for(int c=0;ashkin_teller_channel_ids[c]!=NULL;c++)
				fprintf(out, "<%s> ", ashkin_teller_channel_ids[c]);

			fprintf(out,"\n");
		}
		break;

		default:
		assert(false);
	}
}

void print_summary(struct configuration_t *config,double *wmeans,double *variances,FILE *out)
{
	switch(config->model)
	{
		case MODEL_ISING_NLAYER:
		{
			fprintf(out,"%f %f %f %f ",config->beta,config->Js[0],config->Js[1],config->Jperps[0]);

			for(int c=0;ising_channel_ids[c].id!=NULL;c++)
				if(config->nrlayers>=ising_channel_ids[c].minlayers)
					fprintf(out,"%f ",wmeans[get_channel_nr(ising_channel_ids[c].id)]);

			fprintf(out,"\n");
		}
		break;

		case MODEL_ASHKIN_TELLER:
		{
			fprintf(out,"%f %f %f %f ",config->beta,config->Js[0],config->Js[1],config->Jperps[0]);

			for(int c=0;ashkin_teller_channel_ids[c]!=NULL;c++)
				fprintf(out,"%f ",wmeans[get_channel_nr(ashkin_teller_channel_ids[c])]);

			fprintf(out,"\n");
		}
		break;

		default:
		assert(false);
	}

	fflush(out);
}

void do_mc(struct configuration_t *config,gsl_rng *rng_ctx,FILE *out,bool showheader)
{
	double *wmeans,*variances;
	int total_channels;

	total_channels=get_total_channels();

	/*
		Inizializziamo l'array dove salveremo le medie pesate dei risultati e le
		relative deviazioni standard.
	*/

	wmeans=malloc(sizeof(double)*get_total_channels());
	variances=malloc(sizeof(double)*get_total_channels());

	for(int c=0;c<total_channels;c++)
	{
		wmeans[c]=0.0f;
		variances[c]=0.0f;
	}

	/*
		Scriviamo l'header sul file e siamo pronti!
	*/

	for(int c=0;c<config->runs;c++)
	{
		struct sampling_ctx_t *sctx;
		double *average=malloc(sizeof(double)*get_total_channels());
		double *variance=malloc(sizeof(double)*get_total_channels());

		if(!(sctx=mc_single_run(config,rng_ctx,((c==0)&&(config->savesnapshots==true))?(true):(false))))
			continue;

		sampling_ctx_to_tuple(sctx,average,variance);

		for(int d=0;d<total_channels;d++)
		{
			double weight;

			if(sampling_ctx_get_nr_of_entries(sctx,d)==0)
				continue;

#define ALMOST_ZERO(x)	(fabs(x)<=1e-8)

			if(ALMOST_ZERO(variance[d]))
				variance[d]=1e-5;

			weight=1.0f/variance[d];
			wmeans[d]+=average[d]*weight;
			variances[d]+=weight;
		}

		if(average)
			free(average);

		if(variance)
			free(variance);

		sampling_ctx_fini(sctx);
	}

	/*
		Completiamo il calcolo della media pesata e stampiamo i risultati
	*/

	for(int c=0;c<total_channels;c++)
	{
		/*
			Channels with zero samples will have zero variance.
		*/

		if(ALMOST_ZERO(variances[c]))
			continue;

		wmeans[c]/=variances[c];
		variances[c]=1.0f/variances[c];
	}

#pragma omp critical
	{
		if(showheader==true)
			print_header(config,out);

		print_summary(config,wmeans,variances,out);
	}

	if(wmeans)
		free(wmeans);

	if(variances)
		free(variances);
}
