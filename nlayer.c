#include <assert.h>
#include <stdlib.h>

#include "nlayer.h"
#include "ising.h"

struct nlayer_t *nlayer_init(int x,int y,int nrlayers,const double Js[MAX_NR_OF_LAYERS],const double Jperps[MAX_NR_OF_LAYERS],double beta)
{
	struct nlayer_t *ret;

	assert(x>0);
	assert(y>0);
	assert(nrlayers<MAX_NR_OF_LAYERS);
	assert(beta>0.0f);

	if(!(ret=malloc(sizeof(struct nlayer_t))))
		return NULL;

	ret->lx=x;
	ret->ly=y;
	ret->nrlayers=nrlayers;

	for(int c=0;c<MAX_NR_OF_LAYERS;c++)
	{
		ret->Js[c]=Js[c];
		ret->Jperps[c]=Jperps[c];
	}

	ret->beta=beta;

	for(int c=0;c<nrlayers;c++)
		ret->layers[c]=ising2d_init(x,y);
	
	return ret;
}

void nlayer_fini(struct nlayer_t *nl)
{
	if(nl)
	{
		for(int c=0;c<nl->nrlayers;c++)
			ising2d_fini(nl->layers[c]);

		free(nl);
	}
}

int nlayer_get_spin(struct nlayer_t *nl,int x,int y,int l)
{
	assert(nl!=NULL);
	assert((x>=0)&&(x<nl->lx));
	assert((y>=0)&&(y<nl->lx));
	assert((l>=0)&&(l<nl->nrlayers));

	return ising2d_get_spin(nl->layers[l],x,y);
}

void nlayer_set_spin(struct nlayer_t *nl,int x,int y,int l,int spin)
{
	assert(nl!=NULL);
	assert((x>=0)&&(x<nl->lx));
	assert((y>=0)&&(y<nl->lx));
	assert((l>=0)&&(l<nl->nrlayers));

	assert((spin==1)||(spin==-1));

	ising2d_set_spin(nl->layers[l],x,y,spin);
}

void nlayer_flip_spin(struct nlayer_t *nl,int x,int y,int l)
{
	nlayer_set_spin(nl,x,y,l,-nlayer_get_spin(nl,x,y,l));
}

double nlayer_observable(struct nlayer_t *nl,double (*ising_observable)(struct ising2d_t *))
{
	double ret=0.0f;

	for(int c=0;c<nl->nrlayers;c++)
		ret+=ising_observable(nl->layers[c]);

	return ret;
}

double nlayer_magnetization(struct nlayer_t *nl)
{
	return nlayer_observable(nl,ising2d_magnetization);
}
